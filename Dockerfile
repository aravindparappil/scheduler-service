FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/spring-boot-service.sh"]

COPY spring-boot-service.sh /usr/bin/spring-boot-service.sh
COPY target/spring-boot-service.jar /usr/share/spring-boot-service/spring-boot-service.jar
